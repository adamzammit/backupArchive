# backupArchive

Backup a complete survey archive (LSA/LSS, resources and uploaded responses) from the GUI or via the command line with search options
Will also remove old_ / backup tables to clean up database

## Usage

- Create a directory outside web root
- Set plugin configuration
    - Set complete directory name to _Local directory_
- Backup surveys in bulk locally with PHP Cli:
	- Example 1: Backup, deactivate and archive all surveys that expired more than 3 months ago: <code>php application/commands/console.php plugin --target=backupArchive --function=archive-expired --option="3 months ago"</code>
	- Example 2: Backup, deactivate and archive all surveys created more then 1 year ago: <code>php application/commands/console.php plugin --target=backupArchive --function=archive-created --option="1 year ago"</code>
	- Example 3: Backup, deactivate and archive all surveys with the survey ids 123456, 654321 and 111111: <code>php application/commands/console.php plugin --target=backupArchive --function=archive-listed --option="123456,654321,111111"</code>
- The user can initiate a backup and archive / backup, deactivate and archive from the "Tools" menu
- The user can get the list of files that have been backed up and download them in the "Tools" menu

If you use LimeSurvey version before 3.17.8, you need to be in LimeSurvey directory for call console.php.

## Home page, copyright and support
- HomePage <https://www.acspri.org.au/software/>
- Copyright © 2023 ACSPRI <https://www.acspri.org.au> and [contributors](https://gitlab.com/adamzammit/backupArchive/-/graphs/master)
- Copyright © 2019-2021 Denis Chenu <https://sondages.pro> and [contributors](https://gitlab.com/SondagesPro/coreAndTools/backupSurveyLocally/-/graphs/master)
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- Merge request on [Gitlab](https://gitlab.com/adamzammit/backupArchive/merge_requests)
- Issues on [Gitlab](https://gitlab.com/adamzammit/backupArchive/issues)
- [Professional support](https://www.acspri.org.au/software/)
