<?php

/**
 * backupArchive : Backup and archive a survey
 *
 * @author Adam Zammit <adam.zammit@acspri.org.au>
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2023 ACSPRI <https://www.acspri.org.au>
 * @copyright 2019-2023 Denis Chenu <https://www.sondages.pro>
 * @license AGPL v3
 *
 * @version 1.0.0
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class backupArchive extends PluginBase
{
    // @inheritdoc
    public $allowedPublicMethods = [
        'actionList',
        'actionBackupDeactivateArchiveConfirm',
        'actionBackupDeactivateArchive',
        'actionBackupArchive',
        'actionGet',
    ];
    protected $storage = 'DbStorage';
    protected static $description = 'Backup your survey locally';
    protected static $name = 'backupArchive';

    /**
     * @var array[] the settings
     */
    protected $settings = [
        'localDirectory' => [
            'type' => 'string',
            'label' => 'Local directory where backups will be stored',
            'help' => 'This directory must be out of web access, but the web user must be allowed to create a directory and write in this directory',
        ],
    ];

    // @var bool
    private $echoLog = false;

   // @var string
    private $_errorOnDir = '';

    // @var null|integer
    private $originalErrorReporting = 0;

    /**
     * Add function to be used in cron event.
     *
     * @see parent::init()
     */
    public function init()
    {
        // Action on php cli
        $this->subscribe('direct', 'backupArchiveCLI');
        // Needed config
        $this->subscribe('beforeActivate');
        // The menu
        $this->subscribe('beforeToolsMenuRender');
    }

    /**
     *.
     */
    public function beforeToolsMenuRender()
    {
        $menuEvent = $this->getEvent();
        $surveyId = $menuEvent->get('surveyId');
        // This need export whole right
        if (
            !Permission::model()->hasSurveyPermission($surveyId, 'surveycontent', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'response', 'export')
        ) {
            return;
        }
        $localDirectory = @realpath($localDirectory);
        if (false === $localDirectory) {
            return;
        }
        if (!@is_dir($localDirectory)) {
            return;
        }

        $oSurvey = Survey::model()->findByPk($surveyId);

        $aMenuItems = [
            [
                'label' => $this->gT('Get previous backup'),
                'iconClass' => 'fa fa-download',
                'href' => Yii::app()->createUrl(
                    'admin/pluginhelper',
                    [
                        'sa' => 'sidebody',
                        // ~ 'href' => $url,
                        'plugin' => get_class($this),
                        'method' => 'actionList',
                        'surveyId' => $surveyId,
                    ]
                ),
            ],
        ];
        if ($oSurvey->isActive) {
           $aMenuItems[] = [
               'label' => $this->gT('Backup, deactivate and archive'),
                'iconClass' => 'fa fa-file-archive-o',
                'href' => Yii::app()->createUrl(
                    'admin/pluginhelper',
                    [
                        'sa' => 'sidebody',
                        // ~ 'href' => $url,
                        'plugin' => get_class($this),
                        'method' => 'actionBackupDeactivateArchiveConfirm',
                        'surveyId' => $surveyId,
                    ]
                ),
            ];
        } else {
             $aMenuItems[] = [
                'label' => $this->gT('Backup and archive'),
                'iconClass' => 'fa fa-file-archive-o',
                'href' => Yii::app()->createUrl(
                    'admin/pluginhelper',
                    [
                        'sa' => 'sidebody',
                        // ~ 'href' => $url,
                        'plugin' => get_class($this),
                        'method' => 'actionBackupArchive',
                        'surveyId' => $surveyId,
                    ]
                ),
            ];
           }
        if (class_exists('\\LimeSurvey\\Menu\\MenuItem')) {
            foreach($aMenuItems as $aMenuItem) {
                $menuItems[] = new \LimeSurvey\Menu\MenuItem($aMenuItem);
            }
        } else {
            foreach($aMenuItems as $aMenuItem) {
                $menuItems[] = new \ls\menu\MenuItem($aMenuItem);
            }
        }
        $menuEvent->append('menuItems', $menuItems);
    }

    /**
     *  Deactivate a survey
     *
     *  NOTE: differs from the LimeSurvey core built in deactivate function which
     *  stores "old" data in "old" tables. Here we are deactivating and NOT storing
     *  old data - as we are assuming that a backup was made first and the purpose
     *  of these functions is to reduce database bloat
     *
     * @param mixed $surveyId
     * @param bool $schemaRefresh Do action if true, otherwise dont
     * @param bool $force Do action if true, otherwise just log
     */
    private function ownDeactivate($surveyId,$schemaRefresh=true,$force=true)
    {
        //Could trigger beforeSurveyDeactivate plugin event here if required

        $oSurvey = Survey::model()->findByPk($surveyId);

        $this->backupArchiveLog("Deleting links to CPDB and Saved Control and setting to not active", 2, "ownDeactivate");
        if ($force) {
            //Delete CPDB links if any
            SurveyLink::model()->deleteLinksBySurvey($surveyId);
            SavedControl::model()->deleteSomeRecords(array('sid' => $surveyId));
            $oSurvey->active = 'N';
            $oSurvey->save();
        }

        //Delete tables
        if (tableExists('{{survey_'.$surveyId.'}}')) {
            $this->backupArchiveLog("Dropping survey table for $surveyId", 2, "ownDeactivate");
            if ($force) {
                Yii::app()->db->createCommand()->dropTable("{{survey_".$surveyId."}}");
            }
        }
        if (tableExists('{{survey_'.$surveyId.'_timings}}')) {
            $this->backupArchiveLog("Dropping survey timings table for $surveyId", 2, "ownDeactivate");
            if ($force) {
                Yii::app()->db->createCommand()->dropTable("{{survey_".$surveyId."_timings}}");
            }
        }
        if (tableExists('{{tokens_'.$surveyId.'}}')) {
            $this->backupArchiveLog("Dropping tokens table for $surveyId", 2, "ownDeactivate");
            if ($force) {
                Yii::app()->db->createCommand()->dropTable("{{tokens_".$surveyId."}}");
            }
        }

        //Run a scheme refresh - maybe do this once after total run instead to save cycles
        if ($schemaRefresh) {
            Yii::app()->db->schema->refresh();
        }

        //Could trigger afterSurveyDeactivate plugin event here if required
    }


    /**
     * Delete old tables from the database for surveyId
     * Keep at at least $keep most recent tables (0 to delete all)
     *
     * Typically would keep one if already deactivated so there is some data there to restore
     * If deactivating as well, can delete all as will have a backup of most recent
     *
     * @param mixed $surveyId
     * @param int $keep The number of tables to keep
     * @param bool $force If true: action, otherwise just log
     */
    private function ownDeleteOldTables($surveyId,$keep = 1,$force = true)
    {
        Yii::import('application.helpers.database_helper', true);
        $aQuery[] = [dbSelectTablesLike("{{old_tokens}}_$surveyId" . "%"),''];
        $aQuery[] = [dbSelectTablesLike("{{old_survey}}_$surveyId" . "_timings%"),''];
        $aQuery[] = [dbSelectTablesLike("{{old_survey}}_$surveyId" . "%"),'timings'];
        foreach($aQuery as $sQuery) {
            $aTables = Yii::app()->db->createCommand($sQuery[0])->queryColumn();
            $countt = count($aTables);
            if (!empty($sQuery[1])) { //ignore timings tables in regular search for survey tables
                for ($i =0; $i < $countt; $i++) {
                    if (strpos($aTables[$i],$sQuery[1]) !== false) {
                        unset($aTables[$i]);
                    }
                }
            }
            $tables = count($aTables);
            if ($tables > $keep) {
                asort($aTables); //delete oldest first
                $tcount = 0;
                foreach($aTables as $sTable) {
                    if ($tcount < ($tables - $keep)) {
                        $this->backupArchiveLog("Dropping table $sTable", 2, "ownDeleteOldTables");
                        if ($force) {
                            Yii::app()->db->createCommand()->dropTable($sTable);
                        }
                    }
                    $tcount++;
                }
            }
        }
    }


    public function actionBackupDeactivateArchiveConfirm($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT('This survey does not seem to exist.'));
        }
        if (
            !Permission::model()->hasSurveyPermission($surveyId, 'surveycontent', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'response', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'surveyactivation', 'update')
        ) {
            throw new CHttpException(403);
        }
        // Display confirmation page
        return $this->renderPartial('deactivate', ['surveyid' => $surveyId], true);
    }

    /**
     * Backup, deactive and remove unused database tables (old*)
     *
     * @param mixed $surveyId
     */
    public function actionBackupDeactivateArchive($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT('This survey does not seem to exist.'));
        }
        if ($oSurvey->isActive == false) {
            throw new CHttpException(404, gT('This survey is not currently active.'));
        }

        // This need right to export AND deactivate
        if (
            !Permission::model()->hasSurveyPermission($surveyId, 'surveycontent', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'response', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'surveyactivation', 'update')
        ) {
            throw new CHttpException(403);
        }

        $this->ownLsCommandFix();

        //execute the backup
        $localDirectory = $this->ownCheckDirectory($this->get('localDirectory'));
        if (empty($localDirectory)) {
            $this->backupArchiveLog($this->_errorOnDir, 0, "backupArchive");
            throw new CException($this->_errorOnDir);
        }

        if ($this->ownSaveSurvey($surveyId, $localDirectory)) {
            //backup success - delete ALL old tables as we have backed up current
            $this->ownDeleteOldTables($surveyId,0,false);
            $this->ownDeactivate($surveyId);
        }

        //display backup page
        $redirectUrl = Yii::app()->createAbsoluteUrl('admin/pluginhelper', [
            'sa' => 'sidebody',
            'plugin' => get_class($this),
            'method' => 'actionList',
            'sid' => $surveyId,
        ]);
        Yii::app()->getController()->redirect($redirectUrl);
    }

    /**
     * Backup and remove unused database tables (old*)
     *
     * @param mixed $surveyId
     */
    public function actionBackupArchive($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT('This survey does not seem to exist.'));
        }
        // This need right to export
        if (
            !Permission::model()->hasSurveyPermission($surveyId, 'surveycontent', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'response', 'export')
        ) {
            throw new CHttpException(403);
        }

        $this->ownLsCommandFix();

        //execute the backup
        $localDirectory = $this->ownCheckDirectory($this->get('localDirectory'));
        if (empty($localDirectory)) {
            $this->backupArchiveLog($this->_errorOnDir, 0, "backupArchive");
            throw new CException($this->_errorOnDir);
        }

        if ($this->ownSaveSurvey($surveyId, $localDirectory)) {
            //backup success - delete old tables except most recent
            $this->ownDeleteOldTables($surveyId,1);
        }

        //display backup page
        $redirectUrl = Yii::app()->createAbsoluteUrl('admin/pluginhelper', [
            'sa' => 'sidebody',
            'plugin' => get_class($this),
            'method' => 'actionList',
            'sid' => $surveyId,
        ]);
        Yii::app()->getController()->redirect($redirectUrl);
    }

    /**
     * List already completed backups
     * and give download link.
     *
     * @param mixed $surveyId
     */
    public function actionList($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT('This survey does not seem to exist.'));
        }
        // This need export whole right
        if (
            !Permission::model()->hasSurveyPermission($surveyId, 'surveycontent', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'response', 'export')
        ) {
            throw new CHttpException(403);
        }
        // Get the list
        $aSurveysBackup = $this->getSurveyBackupInformation($surveyId);
        $aData['aSurveysBackup'] = $aSurveysBackup;
        $aData['translation'] = array(
            'No backups exist.' => $this->translate('No backups exist.'),
        );
        $aData['pluginClass'] = get_class($this);
        $aData['surveyId'] = $surveyId;
        $aData['title'] = $this->translate('Backup list');

        return $this->renderPartial('list', $aData, true);
    }


    /**
     * Get the existing backup file for survey in directory
     * @param integer $sureyId
     * @return void
     */
    public function actionGet($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT('This survey does not seem to exist.'));
        }
        // This need export whole right
        if (
            !Permission::model()->hasSurveyPermission($surveyId, 'surveycontent', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'export')
            || !Permission::model()->hasSurveyPermission($surveyId, 'response', 'export')
        ) {
            throw new CHttpException(403);
        }
        $datetime = App()->getRequest()->getQuery('datetime');

        //ensure file exists safely
        $aSurveysBackup = $this->getSurveyBackupInformation($surveyId);
        $currentDir = $this->get('localDirectory') . DIRECTORY_SEPARATOR;

        foreach($aSurveysBackup as $aSurveyBackup) {
                if ($aSurveyBackup['datetime'] == $datetime) {
                    $filename = "survey_complete_archive_" . $surveyId . "-" . $datetime . ".zip";
                    //this file will exist due to check in previous function
                    header("Content-Type: application/zip");
                    header("Content-Disposition: attachment; filename={$filename}");
                    header("Expires: 0");
                    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                    header("Cache-Control: must-revalidate, no-store, no-cache");
                    header("Content-Transfer-Encoding: binary");
                    readfile($currentDir . $filename);
                    App()->end();
                }
        }
        throw new CHttpException(404, "File not found");
    }

    /**
     * get survey archive information
     * @param $surveyId
     * @return array
     */
    private function getSurveyBackupInformation($surveyId)
    {
        $localDirectory = $this->get('localDirectory');
        $aSurveyBackup = array();
        if (@is_dir($localDirectory)) {
            $backupDir = $localDirectory . DIRECTORY_SEPARATOR;
            foreach (glob($backupDir . "survey_complete_archive_{$surveyId}*.zip") as $filename) {
                 preg_match("/survey_complete_archive_" . $surveyId . "-(.*?)\.zip/",$filename, $matches);
                 if (!empty($matches[1])) {
                     $aSurveyBackup[] = [
                        'datetime' => $matches[1],
                    ];
                }
            }
        }
        //put most recent first
        rsort($aSurveyBackup);
        return $aSurveyBackup;
    }

    /**
     * The real action of this plugin, find all survey to be saved
     * Renaming dir one by one
     * Save surveys as LSA in the #1.
     */
    public function backupArchiveCLI()
    {
        $this->echoLog = true;

        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->event->get('target') != get_class()) {
            return;
        }
        if (empty($this->get('localDirectory'))) {
            $this->backupArchiveLog("localDirectory is not set, unable to save your surveys.", 0, "backupArchive");
            throw new CException(
                'localDirectory is not set, unable to save your survey‘s.'
            );
        }
        $this->ownLsCommandFix();

        $localDirectory = $this->ownCheckDirectory($this->get('localDirectory'));
        if (empty($localDirectory)) {
            $this->backupArchiveLog($this->_errorOnDir, 0, "backupArchive");
            throw new CException($this->_errorOnDir);
        }

        $this->ownDisableErrorReporting();

        $criteria = $criteriaO = null;

        switch ($this->event->get('function')) {
            case 'archive-expired'://function archive-expired (archive all expired based on date set in option)
                $strt = strtotime($this->event->get('option'));
                if ($strt !== false) {
                    $criteria = 'expires < :expire';
                    $expd = date('Y-m-d H:i:s', $strt);
                    $criteriaO = ['expire' => $expd];
                    echo "Searching for all surveys that have expired before: $expd \n";
                } else {
                    $this->backupArchiveLog("No valid date provided as an command option", 0, "backupArchive");
                    throw new CException(
                        'No valid date provided as a command option'
                    );
                }
                break;
            case 'archive-created'://function archive-created (archive all based on creation date set in option)
                $strt = strtotime($this->event->get('option'));
                if ($strt !== false) {
                    $criteria = 'datecreated < :created';
                    $crtd = date('Y-m-d H:i:s', $strt);
                    $criteriaO = ['created' => $crtd];
                    echo "Searching for all surveys that were created before: $crtd \n";
                } else {
                    $this->backupArchiveLog("No valid date provided as an command option", 0, "backupArchive");
                    throw new CException(
                        'No valid date provided as a command option'
                    );
                }
                break;
            case 'archive-listed': //function archive-listed (archive based on listed surveyId's - comma separated)
                $aIds = explode(",",$this->event->get('option'));
                $cleanIds = [];
                foreach($aIds as $id) {
                    $tid = intval($id);
                    if ($tid != 0) {
                        $cleanIds[] = intval($id);
                    }
                }
                if (count($cleanIds) > 0) {
                    $criteria = new CDbCriteria;
                    $criteria->addInCondition('sid',$cleanIds);
                } else {
                    $this->backupArchiveLog("No valid survey ids provided as an command option", 0, "backupArchive");
                    throw new CException(
                        'No valid survey ids provided as a command option'
                    );
                }
                break;
            default:
                echo "Please provide a function to execute (one of archive-expired, archive-created, archive-listed)\n";
                echo "archive-expired will backup and archive all surveys expired before a given date (for example: --option=\"3 months ago\")\n";
                echo "archive-created will backup and archive all surveys created before a given date (for example: --option=\"3 months ago\")\n";
                echo "archive-listed will backup and archive all surveys listed by survey id comma separated (for example: --option=\"165356,123456,654433\")\n";
                echo "You will be given the option to cancel (ctrl+c) the execution of the backup and archiving\n";
                return;
        }
        $oSurveysToSave = Survey::model()->findAll($criteria, $criteriaO);

        if (count($oSurveysToSave) > 0) {
            $this->backupArchiveLog("Backup and archive " . count($oSurveysToSave) . " surveys", 2, "backupArchive");

            //dry run first
            foreach ($oSurveysToSave as $oSurvey) {
                $surveyId = $oSurvey->sid;
                echo "Backup $surveyId\n";
                if ($oSurvey->isActive) {
                    $this->ownDeleteOldTables($surveyId,0,false);
                    $this->ownDeactivate($surveyId,false,false);
                } else {
                    $this->ownDeleteOldTables($surveyId,0,false);
                }
            }

            //give 30 seconds to cancel
            echo "If you do not wish to proceed, please cancel now (ctrl+c) - you have 30 seconds\n";
            for ($i = 30; $i > 0; $i--) {
                echo "$i ";
                sleep(1);
            }
            echo "\n";

            foreach ($oSurveysToSave as $oSurvey) {
                $surveyId = $oSurvey->sid;
                echo "Backup $surveyId\n";
                if ($oSurvey->isActive) {
                    if ($this->ownSaveSurvey($surveyId, $localDirectory)) {
                        //backup success - delete ALL old tables as we have backed up current
                        $this->ownDeleteOldTables($surveyId,0,true);
                        $this->ownDeactivate($surveyId,false,true);
                    }
                } else {
                    if ($this->ownSaveSurvey($surveyId, $localDirectory)) {
                        //backup success - delete all but one old table as already deactivated
                        $this->ownDeleteOldTables($surveyId,0,true);
                    }
                }
            }

            //Run once instead of each time deactivation called
            Yii::app()->db->schema->refresh();
        } else {
            echo "No surveys matching criteria\n";
        }

        $this->ownResetServerSettings();
        $this->echoLog = false;
    }

    /**
     * Must set a directory before activate.
     */
    public function beforeActivate()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if (empty($this->get('localDirectory'))) {
            $this->getEvent()->set(
                'message',
                $this->translate(
                    'This plugin must be configured before it can be activated.'
                )
            );
            $this->getEvent()->set('success', false);
            return;
        }
    }

    /**
     * @see parent::saveSettings()
     *
     * @param mixed $settings
     */
    public function saveSettings($settings)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'update')) {
            throw new CHttpException(403);
        }
        if (!empty($settings['localDirectory'])) {
            $checkLocalDirectory = $settings['localDirectory'];
            $settings['localDirectory'] = $this->ownCheckDirectory(
                $settings['localDirectory']
            );
            if (
                $settings['localDirectory']
                && $settings['localDirectory'] != $checkLocalDirectory
            ) {
                App()->setFlashMessage(
                    $this->translate(
                        'The directory was updated to the real path.'
                    ),
                    'warning'
                );
            }
            if (!$settings['localDirectory']) {
                App()->setFlashMessage($this->_errorOnDir, 'danger');
            }
        }
        parent::saveSettings($settings);
    }

    // Set default when show setting
    public function getPluginSettings($getValues = true)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'read')) {
            throw new CHttpException(403);
        }
        $aPluginSettings = parent::getPluginSettings($getValues);
        $aPluginSettings['localDirectory']['help'] = CHtml::tag(
            'p',
            [],
            $this->translate(
                'This directory must be out of web access, but the web user must be allowed to create directory and write in this directory.'
            )
        );
        $aPluginSettings['localDirectory']['help'] .= CHtml::tag(
            'p',
            [],
            sprintf(
                $this->translate('The directory of Limesurvey is : %s'),
                App()->getConfig('rootdir')
            )
        );
        if (
            $getValues
            && !empty($aPluginSettings['localDirectory']['current'])
        ) {
            $checkLocalDirectory =
                $aPluginSettings['localDirectory']['current'];
            $settings['localDirectory'] = $this->ownCheckDirectory(
                $aPluginSettings['localDirectory']['current']
            );
            if (
                $aPluginSettings['localDirectory']['current']
                && $aPluginSettings['localDirectory']['current'] !=
                    $checkLocalDirectory
            ) {
                App()->setFlashMessage(
                    $this->translate(
                        'The directory was updated to the real path.'
                    ),
                    'warning'
                );
            }
            if (!$aPluginSettings['localDirectory']['current']) {
                App()->setFlashMessage($this->_errorOnDir, 'danger');
            }
        }

        return $aPluginSettings;
    }

    /**
     * Check if a directory is valid for purpose.
     *
     * @param mixed $localDirectory
     *
     * @return false|string
     */
    private function ownCheckDirectory($localDirectory)
    {
        // Remove the warning about open_base_dir
        $localDirectory = @realpath($localDirectory);
        if (false === $localDirectory) {
            $this->_errorOnDir = $this->translate(
                'The directory you set is invalid and can not be used.'
            );

            return false;
        }
        rmdirr($localDirectory . '/backupArchiveCheck');
        if (!mkdir($localDirectory . '/backupArchiveCheck')) {
            $this->_errorOnDir = $this->translate(
                'Unable to create directory in directory set.'
            );

            return false;
        }
        // ~ if(!mkdir($localDirectory."/backupArchiveCheck/dir")) {
        // ~ $this->_errorOnDir = $this->translate("Able to create directory in directory set, but unable to create a directory in this new directory.");
        // ~ return false;
        // ~ }
        if (!touch($localDirectory . '/backupArchiveCheck/file')) {
            $this->_errorOnDir = $this->translate(
                'Able to create directory in directory set, but unable to create a file in the directory created.'
            );

            return false;
        }
        if (!rmdirr($localDirectory . '/backupArchiveCheck')) {
            $this->_errorOnDir = $this->translate(
                'Able to create directory in directory set, but unable to delete.'
            );

            return false;
        }
        if (!is_dir($localDirectory . DIRECTORY_SEPARATOR . 'tmp')) {
            mkdir($localDirectory . DIRECTORY_SEPARATOR . 'tmp');
        }

        return $localDirectory;
    }

    /**
     * log an event.
     *
     * @param string $sLog
     * @param int $state error type for Yii log (0: ERROR(error), 1 INFO (warning), 2 DEBUG (info), 3 TRACE (trace))
     * @param string $detail
     * retrun @void
     */
    private function backupArchiveLog($sLog, $state = 0, $detail = "general")
    {
        // Play with DEBUG : ERROR/LOG/DEBUG
        $sNow = date(DATE_ATOM);
        switch ($state) {
            case 0:
                $sLevel = 'error';
                $sLogLog = "[ERROR] {$sLog}";
                break;
            case 1:
                $sLevel = 'warning';
                $sLogLog = "[WARNING] {$sLog}";
                break;
            case 2:
                $sLevel = 'info';
                $sLogLog = "[INFO] {$sLog}";
                break;
            default:
                $sLevel = 'trace';
                $sLogLog = "[DEBUG] {$sLog}";
                break;
        }
        Yii::log($sLog, $sLevel, 'plugin.backupArchiveLog.' . $detail);
        if ($this->echoLog) {
            echo $sLogLog . "\n";
        }
    }

    /**
     * @see parent::gT for LimeSurvey 3.0
     *
     * @param string $sToTranslate The message that are being translated
     * @param string $sEscapeMode  unescaped by default
     * @param string $sLanguage    use current language if is null
     *
     * @return string
     */
    private function translate(
        $sToTranslate,
        $sEscapeMode = 'unescaped',
        $sLanguage = null
    ) {
        if (is_callable($this, 'gT')) {
            return $this->gT($sToTranslate, $sEscapeMode, $sLanguage);
        }

        return $sToTranslate;
    }

    /**
     * Fix LimeSurvey command function and add own needed function.
     *
     * @todo : find way to control API
     * OR if another plugin already fix it
     */
    private function ownLsCommandFix()
    {
        // Bad autoloading in command
        if (!class_exists('DbStorage', false)) {
            include_once dirname(__FILE__) . '/DbStorage.php';
        }
        if (!class_exists('ClassFactory', false)) {
            Yii::import('application.helpers.ClassFactory');
        }

        ClassFactory::registerClass('Token_', 'Token');
        ClassFactory::registerClass('Response_', 'Response');

        // needed for rmdirr function
        Yii::import('application.helpers.common_helper', true);
        // Needed function for export
        Yii::import('application.helpers.export_helper', true);
        Yii::import('application.helpers.globalsettings_helper', true);

        $defaulttheme = Yii::app()->getConfig('defaulttheme');
        // Bad config set for rootdir
        if (
            !is_dir(
                Yii::app()->getConfig('standardthemerootdir') .
                    DIRECTORY_SEPARATOR .
                    $defaulttheme
            )
            && !is_dir(
                Yii::app()->getConfig('userthemerootdir') .
                    DIRECTORY_SEPARATOR .
                    $defaulttheme
            )
        ) {
            /*
             * if still broken : throw error : risk of updating survey when load
             * @see : https://gitlab.com/SondagesPro/mailing/sendMailCron/-/commit/31165bf12201e3dc82b927df095a8f5221bb73d3
             */
            $this->backupArchiveLog("Unable to find default theme {$defaulttheme} in current theme directory", 0, "ownLsCommandFix");
            throw new CException(
                "Unable to find default theme {$defaulttheme} in current theme directory"
            );
        }
    }

    /**
     * Save a complete survey archive as a ZIP file.
     *
     * @param int    $surveyId survey id
     * @param string $dirName  directory name (must exist)
     *
     * @return bool true is survey was saved
     */
    private function ownSaveSurvey($surveyId, $dirName)
    {
        $survey = Survey::model()->findByPk($surveyId);
        if (empty($survey)) {
            $this->backupArchiveLog("Invalid survey id {$surveyId}", 1, "ownSaveSurvey");
            return;
        }

        //create a new ZIP archive containing all survey related materials
        $saveZIPFileName =
            $dirName .
            DIRECTORY_SEPARATOR .
            'survey_complete_archive_' .
            $surveyId . '-' .
            date("Y-m-d-His") .
            '.zip';

        $this->backupArchiveLog("Save complete archive for surveyid {$surveyId}", 3, "ownSaveSurvey");
        // This part is copy /paste from export->_exportarchive core function
        $aSurveyInfo = getSurveyInfo($surveyId);

        $sLSAFileName =
            $dirName .
            DIRECTORY_SEPARATOR .
            'tmp' .
            DIRECTORY_SEPARATOR .
            '.lsa' .
            randomChars(30);
        $sLSSFileName =
            $dirName .
            DIRECTORY_SEPARATOR .
            'tmp' .
            DIRECTORY_SEPARATOR .
            'lss' .
            randomChars(30);
        $sLSRFileName =
            $dirName .
            DIRECTORY_SEPARATOR .
            'tmp' .
            DIRECTORY_SEPARATOR .
            'lsr' .
            randomChars(30);
        $sLSTFileName =
            $dirName .
            DIRECTORY_SEPARATOR .
            'tmp' .
            DIRECTORY_SEPARATOR .
            'lst' .
            randomChars(30);
       $sLSIFileName =
            $dirName .
            DIRECTORY_SEPARATOR .
            'tmp' .
            DIRECTORY_SEPARATOR .
            'lsi' .
            randomChars(30);
       $sResourcesFileName =
            $dirName .
            DIRECTORY_SEPARATOR .
            'tmp' .
            DIRECTORY_SEPARATOR .
            'resources' .
            randomChars(30);

       $sUploadsFileName =
            $dirName .
            DIRECTORY_SEPARATOR .
            'tmp' .
            DIRECTORY_SEPARATOR .
            'uploads' .
            randomChars(30);

        Yii::import('application.libraries.admin.pclzip', true);
        $ziplsa = new PclZip($sLSAFileName);

        touch($sLSSFileName);
        file_put_contents($sLSSFileName, surveyGetXMLData($surveyId));
        $this->ownAddToZip($ziplsa, $sLSSFileName, 'survey_' . $surveyId . '.lss');
        unlink($sLSSFileName);

        if ('Y' == $survey->active) {
            getXMLDataSingleTable(
                $surveyId,
                'survey_' . $surveyId,
                'Responses',
                'responses',
                $sLSRFileName,
                false
            );
            $this->ownAddToZip(
                $ziplsa,
                $sLSRFileName,
                'survey_' . $surveyId . '_responses.lsr'
            );
            unlink($sLSRFileName);
        }

        if (tableExists('{{tokens_' . $surveyId . '}}')) {
            getXMLDataSingleTable(
                $surveyId,
                'tokens_' . $surveyId,
                'Tokens',
                'tokens',
                $sLSTFileName
            );
            $this->ownAddToZip(
                $ziplsa,
                $sLSTFileName,
                'survey_' . $surveyId . '_tokens.lst'
            );
            unlink($sLSTFileName);
        }

        if (tableExists('{{survey_' . $surveyId . '_timings}}')) {
            getXMLDataSingleTable(
                $surveyId,
                'survey_' . $surveyId . '_timings',
                'Timings',
                'timings',
                $sLSIFileName
            );
            $this->ownAddToZip(
                $ziplsa,
                $sLSIFileName,
                'survey_' . $surveyId . '_timings.lsi'
            );
            unlink($sLSIFileName);
        }

        //LSA was a zip file - now add it to the resource ZIP
        $zip = new PclZip($saveZIPFileName);
        $this->ownAddToZip(
            $zip,
            $sLSAFileName,
            'survey_' . $surveyId . '.lsa'
        );
        unlink($sLSAFileName);

        //Add resources to file
        $resourcesdir = Yii::app()->getConfig('uploaddir')."/surveys/{$surveyId}/";
        $zipr = new PclZip($sResourcesFileName);
        $zipdirs = [];
        foreach (array('files', 'flash', 'images') as $zipdir) {
            if (is_dir($resourcesdir.$zipdir)) {
               $zipdirs[] = $resourcesdir.$zipdir.'/';
           }
        }
        $zipr->create($zipdirs, PCLZIP_OPT_REMOVE_PATH, $resourcesdir);

        if (file_exists($sResourcesFileName)) {
            $this->ownAddToZip(
                $zip,
                $sResourcesFileName,
                'resources-survey-' . $surveyId . '.zip'
            );
            unlink($sResourcesFileName);
        }

        //Add uploaded responses to file
        if ('Y' == $survey->active) {
            $responses = Response::model($surveyId)->findAll();
            $rfilelist = [];
            $rfilecount = 0;
            $rtmpdir = Yii::app()->getConfig('uploaddir') . DIRECTORY_SEPARATOR . "surveys" . DIRECTORY_SEPARATOR . $surveyId . DIRECTORY_SEPARATOR . "files" . DIRECTORY_SEPARATOR;
            foreach($responses as $response) {
                foreach($response->getFiles() as $rfile) {
                    $rfilecount++;
                    if (file_exists($rtmpdir . basename($rfile['filename']))) {
                         $rfilelist[] = array($rtmpdir . basename($rfile['filename']),sprintf("%05s_%02s_%s", $response->id, $rfilecount, sanitize_filename(rawurldecode($rfile['name']))));
                    }
                }
            }
            //if there are uploaded responses - add them
            if (count($rfilelist) > 0) {
                $zipu = new PclZip($sUploadsFileName);
                foreach($rfilelist as $file) {
                    $this->ownAddToZip(
                        $zipu,
                        $file[0],
                        $file[1]
                    );
                }
                $this->ownAddToZip(
                    $zip,
                    $sUploadsFileName,
                    "Files_for_survey_$surveyId.zip"
                );
                unlink($sUploadsFileName);
            }
        }

        $this->backupArchiveLog("Complete archive for surveyid {$surveyId} saved", 2, "ownSaveSurvey");
        return true;
    }

    /**
     * Disable error if needed
     * @return @void
     */
    private function ownDisableErrorReporting()
    {
        $this->originalErrorReporting = error_reporting();
        if (!defined('YII_DEBUG')) {
            define('YII_DEBUG', false);
        }
        error_reporting(0);
    }

    /**
     * Reenable error if needed
     * @return @void
     */
    private function ownResetServerSettings()
    {
        if($this->originalErrorReporting) {
            error_reporting($this->originalErrorReporting);
        }
    }

    /**
     * Function copy from export.
     *
     * @see \export->_addToZip
     *
     * @param PclZip $zip
     * @param string $name
     * @param string $full_name
     */
    private function ownAddToZip($zip, $name, $full_name)
    {
        $check = $zip->add([
            [
                PCLZIP_ATT_FILE_NAME => $name,
                PCLZIP_ATT_FILE_NEW_FULL_NAME => $full_name,
            ],
        ]);
        if (0 == $check) {
            $this->backupArchiveLog("PCLZip error : "  . $zip->errorInfo(true) , 0, "ownAddToZip");
        }
    }
}
