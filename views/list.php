<div class="row">
    <div class="col-lg-12 content-right">
        <?php if ($title) {
            echo CHtml::tag(
                "h3",
                array(
                    'id' => 'title-form-' . $pluginClass
                ),
                $title
            );
        } ?>
    </div>
    <?php if (empty($aSurveysBackup)) : ?>
        <p><?= $translation['No backups exist.']; ?></p>
    <?php else: ?>
        <ul>
            <?php foreach ($aSurveysBackup as $aSurveyBackup) : ?>
            <li>
                <?php
                $datetime = $aSurveyBackup['datetime'];
                $urllabel = "Complete survey archive at $datetime";
                echo Chtml::link(
                    $urllabel,
                    array(
                        'admin/pluginhelper',
                        'sa' => 'sidebody',
                        'plugin' => $pluginClass,
                        'method' => 'actionGet',
                        'surveyId' => $surveyId,
                        'datetime' => $datetime
                    )
                );
                ?>
            </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>

