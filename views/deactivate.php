<div class='side-body <?php echo getSideBodyClass(false); ?>'>
    <div class="row welcome survey-action">
        <div class="col-sm-12 content-right">
            <div class="jumbotron message-box message-box-error">
                <h2>
                    <?php eT("Backup and Deactivate this survey");  echo "<em>($surveyid)</em>" ; ?>
                </h2>
                <p class="lead text-warning">
                    <?php eT("Warning: Please read this carefully before proceeding!"); ?>
                </p>
                <ul>
                    <li><?php eT("All responses are not accessible anymore with LimeSurvey.");?> <?php echo gT("Your response table will backed up but would need to be restored to access responses"); ?></li>
                    <li><?php eT("All participant information is backed up but no longer accessible without restoring.");?></li>
                    <li><?php eT("A deactivated survey is not accessible to participants (only a message appears that they are not permitted to see this survey).");?></li>
                    <li><?php eT("All questions, groups and parameters are editable again.");?></li>
                </ul>
                <?php echo CHtml::form(array("admin/pluginhelper/sa/sidebody/plugin/backupArchive/method/actionBackupDeactivateArchive/surveyId/{$surveyid}/"), 'post'); ?>
                <p><input  class="btn btn-default" type='submit' value='<?php eT("Backup and Deactivate survey"); ?>'/></p>
                <input type='hidden' value='Y' name='ok' />
                </form>
            </div>

            <div class="jumbotron message-box" style="border: none;">
                <a href="<?php echo Yii::app()->createUrl('admin/survey/sa/view/surveyid/'.$surveyid);?>" class="btn btn-danger btn-lg">
                    <?php eT('Cancel');?>
                </a>
            </div>
        </div>
    </div>
</div>
